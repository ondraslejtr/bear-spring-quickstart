![Coding Bear logo](coding-bear/logo.png)

# Bear Spring Quickstart

**Bear Spring Quickstart** is a no-nonsense Spring project skeleton that includes all basic libraries and configurations
that Coding Bear programmers find useful for quick spin-up of a new project.

## Quick feature overview

* Gradle wrapper
* Kotlin based Gradle build scripts
* Custom-built .gitignore
* ...

## Project content overview

### Sample application

* Sample Spring Boot application with sample user endpoint.
* Prepared connection to PostgreSQL database through Spring JPA

### Config

#### Development database

Project includes Gradle plugin that allows simple spin-ups of docker-compose containing PostgreSQL instance by running Gradle task.

To change container settings, simply edit YAML config file in `/config/docker/database-compose.yml`. These changes should then be propagated to Hibernate configuration in all applications depending on this database (namely `application.yml` config files in resource folder of each runnable project).

All configuration happens in `/config/docker/database-compose.yml` file. It obeys [standard compose file structure](https://docs.docker.com/compose/compose-file/).

Hook up of Gradle plugin happens in `/build.gradle.kts` script.

## Running the project

### Develpoment PostgreSQL database

```bash
./gradlew composeUp
```


### Sample project

Sample project depends on having database running, so remember to spin it up (project includes option to run instance in Docker container included as Gradle task in root build script - see [Running PostgreSQL database](#postgresql-database)).

## About Coding Bear

* TODO: Fill in
* Why we made this project and what is it to us
* What do we do - what can we do for you
* Contact

## Adjusting skeleton for your needs

### Changing group ID

### Changing package names

#### IntelliJ IDEA

* Select your 