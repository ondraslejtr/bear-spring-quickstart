dependencies {
    compile("org.springframework.boot", "spring-boot-starter-web")
    compile("org.springframework.boot", "spring-boot-starter-data-jpa")
    compile("org.postgresql", "postgresql")
}