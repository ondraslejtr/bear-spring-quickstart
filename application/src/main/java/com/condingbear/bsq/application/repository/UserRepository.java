package com.condingbear.bsq.application.repository;

import com.condingbear.bsq.application.entity.TestUser;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserRepository extends PagingAndSortingRepository<TestUser, Integer> {


}
