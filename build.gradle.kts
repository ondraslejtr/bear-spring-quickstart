plugins {
    id("org.springframework.boot") version "2.1.1.RELEASE" apply false
    id("io.spring.dependency-management") version "1.0.6.RELEASE"
    id("com.avast.gradle.docker-compose") version "0.8.12"
    java
}

group = "cz.codingbear"
version = "1.0"

dockerCompose {
    useComposeFiles = listOf("config/docker/environment-compose.yml")
}

allprojects {
    group = "cz.codingbear"
    version = "$version"
}

subprojects {
    plugins.apply("io.spring.dependency-management")
    plugins.apply("org.springframework.boot")
    plugins.apply("java")

    repositories {
        mavenCentral()
    }

    java {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

}
